var host="http://localhost:8000/"
var ofertes;
var categotia={
    DAW:0,
    DAM:0,
    ASIX:0,
    SMIX:0
}
get_ofertes = function () {
    axios.get(host + "mostrarOfertesUsuari")
        .then(function (response) {
            ofertes = response.data;
            contartCategoria();

        })
        .catch(function (error) {
            console.log(error);
        })
}

contartCategoria = function(){
    categotia.DAW = ofertes.filter(list => list.categoria.nombre == "DAW").length;
    categotia.DAM = ofertes.filter(list => list.categoria.nombre == "DAM").length ;
    categotia.ASIX = ofertes.filter(list => list.categoria.nombre == "ASIX").length ;
    categotia.SMIX = ofertes.filter(list => list.categoria.nombre == "SMIX").length ;
}

get_ofertes();


anychart.onDocumentLoad(function () {
    // create an instance of a pie chart
    let chart = anychart.pie();
    // set the data
    chart.data([
        ["DAW",  categotia.DAW],
        ["DAM",  categotia.DAM],
        ["ASIX", categotia.ASIX],
        ["SMIX", categotia.SMIX]
    ]);
    // set chart title
    chart.title("Ofertes de feina");
    // set the container element
    chart.container("container");
    // initiate chart display
    chart.draw();
});


var app = new Vue({
    el: '#app',
    data: {
        v_host:"http://localhost:8000/",
        v_categotia:{
            DAW:0,
            DAM:0,
            ASIX:0,
            SMIX:0
        }
    },
    mounted: function () {
        this.get_ofertes();
    },
    methods: {
        get_ofertes:function () {
            var reference = this;
            axios.get(this.v_host + "mostrarOfertesUsuari")
                .then(function (response) {
                    reference.v_contartCategoria(response.data);
                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        v_contartCategoria : function(listaOfertas){
            this.v_categotia.DAW = (listaOfertas.filter(list => list.categoria.nombre == "DAW").length);
            this.v_categotia.DAM = (listaOfertas.filter(list => list.categoria.nombre == "DAM").length);
            this.v_categotia.ASIX = (listaOfertas.filter(list => list.categoria.nombre == "ASIX").length);
            this.v_categotia.SMIX = (listaOfertas.filter(list => list.categoria.nombre == "SMIX").length);
        }
    }
})