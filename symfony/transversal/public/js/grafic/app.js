var app = new Vue({
    el: '#app',
    data: {
        ofertaList: [],
        ofertaListCopy: [],
        dataActual:new Date(),
        host:"http://localhost:8000/",
        count:10,
    },
    mounted: function () {
        this.get_ofertes();
        this.pintarGrafic();
    },
    methods: {
        get_ofertes: function () {
            var referent = this;
            axios.get(this.host + "mostrarOfertesUsuari")
                .then(function (response) {
                    referent.ofertaList = response.data;

                    for (let i = 0; i<  referent.ofertaList.length;i++){
                        //format de data
                        referent.ofertaList[i].data_publicacio.date =new Date( referent.ofertaList[i].data_publicacio.date).toLocaleDateString();

                        //afagir atribut per controla si es obra la oferta.
                        referent.ofertaList[i].active = false;

                        //afegir la atribut cartaPresentacio
                        referent.ofertaList[i].cartaPresentacio = "";
                    }
                    //si es mayor de 15 dies
                    for (let i = 0; i<  referent.ofertaList.length;i++){
                        referent.ofertaList[i].antiguitat = referent.addDay(new Date(referent.ofertaList[i].data_publicacio.date),15) > new Date();
                    }
                    referent.ofertaListCopy = referent.ofertaList;
                    referent.ofertaListCopy[0].active = true;

                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        addDay: function(date,days) {
            date.setDate(date.getDate() + days);
            return date;
        },
        filterCategoria:function (categoria) {
            this.ofertaListCopy=[]
            if(categoria == "tot"){
                this.ofertaListCopy = this.ofertaList;
            } else{
                this.ofertaListCopy  = this.ofertaList.filter(list => list.categoria.nombre == categoria);
            }
        },
        avtivar:function (index) {
            this.ofertaListCopy[index].active = !this.ofertaListCopy[index].active;

        },
        inscribir:function(oferta) {

            axios.post(this.host+"inscriureOferta", {
                numeroId: `${this.user.id}`,
                idOferta: `${oferta.id}`,
                cartaPresentacio:`${oferta.cartaPresentacio}`
            })
                .then(function (response) {
                    console.log(response);
                })
                .catch(function (error) {
                    console.log(error);
                });
        },
        pintarGrafic:function() {
        }
    }
})
