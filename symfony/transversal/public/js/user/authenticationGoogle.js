/*
function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    window.location.href = `http://localhost:8000/comprovarUsuari/${profile.getId()}/${profile.getName()}/${profile.getEmail()}`;
    console.log(`http://localhost:8000/comprovarUsuari/${profile.getId()}/${profile.getName()}/${profile.getEmail()}`)
}*/

var user={};

function onLoadGoogleCallback(){
    gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
            client_id: '283993195253-7bs3dgb6k0pqbhvidc26mu9im3ihhj0r.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            scope: 'profile'
        });

        auth2.attachClickHandler(element, {},
            function(googleUser) {
                var profile = googleUser.getBasicProfile();
                console.log('Signed in: ' + googleUser.getBasicProfile().getName());
                window.location.href = `http://localhost:8000/comprovarUsuari/${profile.getId()}/${profile.getName()}/${profile.getEmail()}`;
                user = {
                    'id':`${profile.getId()}`,
                    'name':`${profile.getName()}`,
                    'email':`${profile.getEmail()}`,
                    'ImageURL':`${profile.getImageUrl()}`
                };
                sessionStorage.setItem('user', JSON.stringify(user));


            }, function(error) {
                console.log('Sign-in error', error);
            }
        );
    });

    element = document.getElementById('googleSignIn');
}


function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.disconnect().then(function () {
        console.log('User signed out.');
    });
    sessionStorage.clear()
    window.location.href  = "http://localhost:8000/index";

}
