/*function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}*/


var app = new Vue({
    el: '#app',
    data: {
        ofertaList: [],
        ofertaListCopy: [],
        dataActual:new Date(),
        host:"http://localhost:8000/",
        user: JSON.parse(sessionStorage.user),
    },
    mounted: function () {
        this.get_ofertes();
    },
    methods: {
        get_ofertes: function () {
            var referent = this;
            axios.get(this.host + "mostrarOfertesUsuari")
                .then(function (response) {
                    referent.ofertaList = response.data;

                    for (let i = 0; i<  referent.ofertaList.length;i++){
                        //format de data
                        referent.ofertaList[i].data_publicacio.date =new Date( referent.ofertaList[i].data_publicacio.date).toLocaleDateString();

                        //afagir atribut per controla si es obra la oferta.
                        referent.ofertaList[i].active = false;

                        //afegir la atribut cartaPresentacio
                        referent.ofertaList[i].cartaPresentacio = "";

                        //afegir checkbox
                        referent.ofertaList[i].validar = "";
                        //afegir checkbox
                        referent.ofertaList[i].validada = false;
                    }
                    //si es mayor de 15 dies
                    for (let i = 0; i<  referent.ofertaList.length;i++){
                        referent.ofertaList[i].antiguitat = referent.addDay(new Date(referent.ofertaList[i].data_publicacio.date),15) > new Date();
                    }
                    referent.ofertaListCopy = referent.ofertaList;
                    referent.ofertaListCopy[0].active = true;
                    referent.get_ofertes_inscritas(referent.user.id);

                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        get_ofertes_inscritas: function (userId) {
            var referent = this;
            axios.get(this.host + "mostrarOfertesInscritas/"+ userId)
                .then(function (response) {
                    referent.candidatList = response.data;


                    for (let i = 0; i<  referent.candidatList.length;i++){
                        for (let j = 0; j<  referent.ofertaList.length;j++){
                            if(referent.candidatList[i].id_oferta.id == referent.ofertaList[j].id) {
                                referent.ofertaListCopy[j].validada = true;
                            }
                        }
                    }

                })
                .catch(function (error) {
                    console.log(error);
                })
        },
        addDay: function(date,days) {
            date.setDate(date.getDate() + days);
            return date;
        },
        filterCategoria:function (categoria) {
            this.ofertaListCopy=[]
            if(categoria == "tot"){
                this.ofertaListCopy = this.ofertaList;
            } else{
                this.ofertaListCopy  = this.ofertaList.filter(list => list.categoria.nombre == categoria);
            }
        },
        avtivar:function (index) {
            this.ofertaListCopy[index].active = !this.ofertaListCopy[index].active;

        },
        inscribir:function(oferta) {

            if(`${oferta.validar}`== `true`){
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000
                });

                Toast.fire({
                    type: 'success',
                    title: "T'has inscrit a l'oferta"
                })

                this.get_ofertes();

                axios.post(this.host+"inscriureOferta", {
                    numeroId: `${this.user.id}`,
                    idOferta: `${oferta.id}`,
                    cartaPresentacio:`${oferta.cartaPresentacio}`
                })
                    .then(function (response) {
                        //console.log(response);

                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
            else {
                alert("Per poder inscriure en l'oferta primer has d'acceptar l'autorització");
            }


        }
    }
})
