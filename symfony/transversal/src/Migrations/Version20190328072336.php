<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190328072336 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE candidat_oferta (id INT AUTO_INCREMENT NOT NULL, id_oferta_id INT NOT NULL, username_id INT NOT NULL, data_inscripcio DATETIME NOT NULL, carta_presentacio VARCHAR(300) DEFAULT NULL, ip VARCHAR(255) DEFAULT NULL, INDEX IDX_CAB2358E7ACE4A03 (id_oferta_id), INDEX IDX_CAB2358EED766068 (username_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidat (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(50) NOT NULL, numero_id VARCHAR(255) NOT NULL, link_cv VARCHAR(100) DEFAULT NULL, correu VARCHAR(60) DEFAULT NULL, rol VARCHAR(20) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE candidat_oferta ADD CONSTRAINT FK_CAB2358E7ACE4A03 FOREIGN KEY (id_oferta_id) REFERENCES ofertes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE candidat_oferta ADD CONSTRAINT FK_CAB2358EED766068 FOREIGN KEY (username_id) REFERENCES candidat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ofertes CHANGE inscrits inscrits INT DEFAULT 0 NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidat_oferta DROP FOREIGN KEY FK_CAB2358EED766068');
        $this->addSql('DROP TABLE candidat_oferta');
        $this->addSql('DROP TABLE candidat');
        $this->addSql('ALTER TABLE ofertes CHANGE inscrits inscrits INT NOT NULL');
    }
}
