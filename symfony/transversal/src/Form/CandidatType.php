<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 21/03/19
 * Time: 20:42
 */

namespace App\Form;

use App\Entity\Candidat;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class CandidatType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                // ...
            ->add('brochure', FileType::class, ['label' => 'Brochure (PDF file)'])
                // ...
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'cv_link' => Candidat::class,
        ]);
    }

}