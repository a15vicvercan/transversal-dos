<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 23/03/19
 * Time: 10:02
 */

namespace App\Controller;

use http\QueryString;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\CandidatType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use AppBundle\Entity\User;
use App\Entity\UserType;
use App\Entity\Ofertes;
use App\Entity\Candidat;
use App\Entity\CandidatOferta;

use App\Entity\Categoria;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Translation\Tests\StringClass;


class MailController extends AbstractController {

    //Esta funcion se encargar de enviar la empresa que ha creado la oferta, un email cada vez que algun candidato de
    //inscribe en la oferta con sus datos, CV y carta de presentacion (si tiene).
    /**
     * @Route("/enviarMail/{idOferta}/{idUsuari}", name="enviarMail")
     */
    public function EnviarMail($idOferta, $idUsuari, \Swift_Mailer $mailer)
    {

        $found = $this->getDoctrine()
            ->getRepository(Candidat::class)
            ->createQueryBuilder('candidat')
            ->addSelect('candidat')
            ->andWhere('candidat.numeroId = :id')
            ->setParameter('id', $idUsuari)
            ->getQuery();

        $candidat = $found->getResult();

        $found = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->createQueryBuilder('oferta')
            ->addSelect('oferta')
            ->andWhere('oferta.id = :id')
            ->setParameter('id', $idOferta)
            ->getQuery();

        $empresa = $found->getResult();

        $found = $this->getDoctrine()
            ->getRepository(CandidatOferta::class)
            ->createQueryBuilder('candidatOferta')
            ->addSelect('candidatOferta')
            ->where('candidatOferta.username = :id')
            ->setParameter('id', $candidat[0]->getId())
            ->andWhere('candidatOferta.id_oferta = :idOferta')
            ->setParameter('idOferta', $idOferta)
            ->getQuery();

        $candidatOferta = $found->getResult();

        $nom = $candidat[0]->getUsername();
        $curriculum = $candidat[0]->getLinkCv();
        $correu = $candidat[0]->getCorreu();
        $cartaPresentacio = $candidatOferta[0]->getCartaPresentacio();

        $mailEmpresa = $empresa[0]->getCorreo();
        $titolOferta = $empresa[0]->getTitol();

        //Creamos el mail con los datos de la oferta y del candidato obtenidos anteriormente
        $message = (new \Swift_Message("Nova inscripció a l'oferta"))
            ->setFrom('iamworking2019@gmail.com')
            ->setTo($mailEmpresa)
            ->setBody(
                $this->renderView(
                // templates/emails/mail.html.twig
                    'Mail/mail.html.twig',
                    ['nom' => $nom,
                     'titolOferta' => $titolOferta,
                     'correu' => $correu,
                     'cartaPresentacio' => $cartaPresentacio
                    ]
                ),
                'text/html'
            )
            ->attach(\Swift_Attachment::fromPath('%kernel.project_dir%/curriculums/'.$curriculum));

        $mailer->send($message);

        return $this->redirectToRoute('mostrarOfertasSinValidar');
    }

}