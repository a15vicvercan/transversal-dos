<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 23/03/19
 * Time: 9:49
 */

namespace App\Controller;

use http\QueryString;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\CandidatType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use AppBundle\Entity\User;
use App\Entity\UserType;
use App\Entity\Ofertes;
use App\Entity\Candidat;
use App\Entity\CandidatOferta;

use App\Entity\Categoria;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Translation\Tests\StringClass;


class AdminController extends AbstractController {

    /**
     * @Route("/mostrarOfertesValidades", name="mostrarOfertesValidadas")
     */
    public function MostrarOfertesValidadas()
    {
        // creates a task and gives it some dummy data for this example
        $repo = $this->getDoctrine()->getRepository(Ofertes::class);

        $found = $repo->findBy(
            array(
                'validada' => '1',
            ),
            array('data_publicacio' => 'DESC')
        );

        if($found){
            return $this->render('Admin/listar.html.twig', [
                'ofertes' => $found,
                'origen' => 'mostrarOfertasValidadas'
            ]);

        }

        if (!$found) {
            throw $this->createNotFoundException(
                'No hi ha cap oferta validada'
            );
        }
    }

    /**
     * @Route("/mostrarOfertasSinValidar", name="mostrarOfertasSinValidar")
     */
    public function MostrarOfertesSenseValidar()
    {
        // creates a task and gives it some dummy data for this example
        $repo = $this->getDoctrine()->getRepository(Ofertes::class);

        // this returns a single item
        $found = $repo->findBy(
            array(
                'validada' => '0',
            ),
            array('data_publicacio' => 'DESC')
        );

        return $this->render('Admin/listarNoValidades.html.twig', [
            'ofertes' => $found,
            'origen' => 'mostrarOfertas'
        ]);
    }

    /**
     * @Route("/validarOferta/{id}", name="validarOferta")
     */
    public function ValidarOferta($id)
    {
        // creates a task and gives it some dummy data for this example
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Ofertes::class)->find($id);

        $article->setValidada(1);

        $entityManager->flush();

        return $this->redirectToRoute('mostrarOfertasSinValidar');

    }

    /**
     * @Route("/descartarOferta/{id}", name="descartarOferta")
     */
    public function DescartarOferta($id)
    {
        // creates a task and gives it some dummy data for this example
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Ofertes::class)->find($id);

        $entityManager->remove($article);

        $entityManager->flush();

        return $this->redirectToRoute('mostrarOfertasSinValidar');

    }

    /**
     * @Route("/editarOferta/{id}", name="editarOferta")
     */
    public function editarOferta($id,Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $ofertes = $entityManager->getRepository(Ofertes::class)->find($id);
        // con el set se ponen los campos por defecto
        $form = $this->createFormBuilder($ofertes)
            ->add('titol', TextType::class)
            ->add('descripcio', TextareaType::class)
            ->add('categoria', EntityType::class, array(
                'class' => 'App\Entity\Categoria',
                'choice_label' => 'nombre'
            ))
            ->add('submit', SubmitType::class, ['label' => 'Guardar Oferta'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $oferta = $form->getData();
            $entityManager->persist($oferta);
            $entityManager->flush();

            return $this->redirectToRoute('mostrarOfertesValidadas');

        }


        return $this->render('Ofertes/editOferta.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/inscritsOferta/{id}", name="inscritsOferta", methods={"GET","HEAD"})
     */
    public function inscritsOferta($id,Request $request)
    {
        $found = $this->getDoctrine()
            ->getRepository(CandidatOferta::class)
            ->createQueryBuilder('candidatOferta')
            ->innerJoin('candidatOferta.username', 'username')
            ->addSelect('username')
            ->where('candidatOferta.id_oferta = :id')
            ->setParameter('id', $id)
            ->getQuery();

        $candidatOferta = $found->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        return $this->render('Admin/inscrits.html.twig', [
            'candidats' => $candidatOferta,
            'inscrits' => count($candidatOferta)
        ]);
    }

}