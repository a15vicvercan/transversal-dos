<?php
// src/Controller/OfertesController.php
namespace App\Controller;

use http\QueryString;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\CandidatType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use AppBundle\Entity\User;
use App\Entity\UserType;
use App\Entity\Ofertes;
use App\Entity\Candidat;

use App\Entity\Categoria;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Translation\Tests\StringClass;

class OfertesController extends AbstractController {

    /**
     * @Route("/index", name="index")
     */
    public function index()
    {

        return $this->render('index.html.twig');
    }

    //Funcion con la que las empresas pueden crear las ofertas
    /**
     * @Route("/novaOferta", name="novaOferta")
     */
    public function AfegirOferta(Request $request)
    {
        $ofertes = new Ofertes();

        // con el set se ponen los campos por defecto
        $ofertes->setDataPublicacio(new \DateTime("now"));
        $ofertes->setValidada(0);
        $ofertes->setInscrits(0);

        $form = $this->createFormBuilder($ofertes)
            ->add('titol', TextType::class)
            ->add('descripcio', TextareaType::class)
            ->add('ubicacio', TextType::class)
            ->add('nombre_empresa', TextType::class)
            ->add('correo', EmailType::class)
            ->add('categoria', EntityType::class, array(
                'class' => 'App\Entity\Categoria',
                'choice_label' => 'nombre'
            ))
            ->add('submit', SubmitType::class, ['label' => 'Crear Oferta'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $oferta = $form->getData();
            $entityManager->persist($oferta);
            $entityManager->flush();
            return $this->redirectToRoute('novaOferta');
        }

        return $this->render('Ofertes/newOferta.twig', [
            'form' => $form->createView(),
            'origen' => 'novaOferta'
        ]);
    }

}
