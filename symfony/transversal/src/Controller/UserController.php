<?php
/**
 * Created by PhpStorm.
 * User: ausias
 * Date: 21/03/19
 * Time: 13:07
 */
namespace App\Controller;

use App\Entity\CandidatOferta;
use http\QueryString;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\CandidatType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use AppBundle\Entity\User;
use App\Entity\UserType;
use App\Entity\Ofertes;
use App\Entity\Candidat;
use App\Entity\Categoria;
use App\Controller\Post;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
// Include Dompdf required namespaces
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Translation\Tests\StringClass;

class UserController extends AbstractController {

    //Funcion que muestra las ofertas al usuario. Tambien contiene el formulario para poder subir el CV
    /**
     * @Route("/indexUser/{id}", name="indexUser")
     */
    public function  index($id, Request $request){
        $form = $this->createFormBuilder()
            ->add('link_cv', FileType::class, array(
                "label" => "Arxiu:",
                "attr" =>array("class" => "Candidat")
            ))
            ->add('submit', SubmitType::class, ['label' => 'Pujar curriculum'])
            ->getForm();


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            $candidat = $entityManager->getRepository(Candidat::class)->findBy(["numeroId"=>$id]);

            // Recogemos el fichero
            $file=$form['link_cv']->getData();

            // Sacamos la extensión del fichero
            $ext=$file->guessExtension();

            // Le ponemos un nombre al fichero
            $file_name=time().".".$ext;

            // Guardamos el fichero en el directorio uploads que estará en el directorio /web del framework
            $file->move('%kernel.project_dir%/curriculums/', $file_name);

            // Establecemos el nombre de fichero en el atributo de la entidad
            $candidat[0]->setLinkCv($file_name);

            $entityManager->persist($candidat[0]);
            $entityManager->flush();

        }

        return $this->render('User/UserIndex.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    //API GET que devuelve todas las ofertas validadas y con una antiguedad maximo de 3 meses en un JSON
    /**
     * @Route("/mostrarOfertesUsuari", name="mostrarOfertesUsuari", methods={"GET","HEAD"})
     */
    public function mostrarOfertesUsuari(Request $request)
    {
        $date = new \DateTime();
        $date->modify('-3 months');

        $ofertes = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->createQueryBuilder('oferta')
            ->innerJoin('oferta.categoria', 'categoria')
            ->addSelect('categoria')
            ->where('oferta.categoria_id = :id')
            ->where('oferta.validada = 1')
            ->andWhere('oferta.data_publicacio > :tresmesos')
            ->setParameter('tresmesos', $date)
            ->addOrderBy('oferta.data_publicacio','DESC')
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)
        ;

        return new JsonResponse($ofertes);
    }

    //API GET a la que le pasamos un ID de usuario y nos devuelve un JSON con todas las ofertas en la que el
    //usuario esta inscrito
    /**
     * @Route("/mostrarOfertesInscritas/{id}", name="mostrarOfertesInscritas", methods={"GET","HEAD"})
     */
    public function mostrarOfertesInscritas($id, Request $request)
    {

        $candidat = $this->getDoctrine()
            ->getRepository(Candidat::class)
            ->createQueryBuilder('candidat')
            ->addSelect('candidat')
            ->where('candidat.numeroId = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;

        $ofertes = $this->getDoctrine()
            ->getRepository(CandidatOferta::class)
            ->createQueryBuilder('oferta')
            ->innerJoin('oferta.id_oferta', 'idOfertaInscrita')
            ->innerJoin('oferta.username', 'userInscrito')
            ->addSelect('idOfertaInscrita, userInscrito')
            ->where('oferta.username = :id')
            ->setParameter('id', $candidat[0]->getId())
            ->getQuery()
            ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)
        ;

        return new JsonResponse($ofertes);
    }

    //API POST a la que se le pasa un fichero JSON con el ID del candidato, el ID de oferta y la carta de presentacion
    //para poder inscribir al candidato a una oferta
    /**
     * @Route("/inscriureOferta", name="inscriureOferta", methods={"POST"})
     */
    public function inscriureAOferta(Request $request)
    {
        //JSON_decode transforma el fichero JSON que se le pasa y lo convierte en un array
        $data = json_decode($request->getContent(), true);

        $found = $this->getDoctrine()
            ->getRepository(Candidat::class)
            ->createQueryBuilder('candidat')
            ->addSelect('candidat')
            ->andWhere('candidat.numeroId = :id')
            ->setParameter('id', $data['numeroId'])
            ->getQuery();

        $candidat = $found->getResult();

        $found = $this->getDoctrine()
            ->getRepository(Ofertes::class)
            ->createQueryBuilder('oferta')
            ->addSelect('oferta')
            ->andWhere('oferta.id = :id')
            ->setParameter('id', $data['idOferta'])
            ->getQuery();

        $oferta = $found->getResult();

        $candidatOferta = new CandidatOferta();

        $ipCliente = $this->container->get('request_stack')->getCurrentRequest()->getClientIp();

        // con el set se ponen los campos por defecto
        $candidatOferta->setDataInscripcio(new \DateTime("now"));
        $candidatOferta->setIp($ipCliente);
        $candidatOferta->setUsername($candidat[0]);
        $candidatOferta->setIdOferta($oferta[0]);
        $candidatOferta->setCartaPresentacio($data['cartaPresentacio']);

        $idOferta = $oferta[0]->getId();
        $idUsuari = $candidat[0]->getNumeroId();

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($candidatOferta);
        $entityManager->flush();

        return $this->redirectToRoute('enviarMail', ['idOferta'=>$idOferta, 'idUsuari'=>$idUsuari]);

    }

    //Funcion que se ejecuta al hacer el login para comprobar si el usuario existe o no
    /**
     * @Route("/comprovarUsuari/{id}/{username}/{correu}", name="comprovarUsuari")
     */
    public function comprovarUsuari($id, $username, $correu, Request $request)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $candidat = $entityManager->getRepository(Candidat::class)->findBy(["numeroId" => $id]);

        //Si el usuario que hace login no existe, lo crea en la table candidat de la BBDD
        if (!$candidat) {
            $entityManager = $this->getDoctrine()->getManager();

            $candidat = new Candidat();

            $candidat->setUsername($username);
            $candidat->setNumeroId($id);
            $candidat->setCorreu($correu);
            //Por defecto se le pone el rol de Usuario
            $candidat->setRol("user");

            $entityManager->persist($candidat);
            $entityManager->flush();
            return $this->redirectToRoute('indexUser', ['id'=>$id]);

        }

        $rol = $candidat[0]->getRol();

        $usuari = "user";
        $admin = "admin";

        //Si el usuario que hace login tiene el rol de Usuario, se le redirige automaticamente a mostrarle las ofertas,
        //si el usuario tiene el rol de Administrador, se le redirige al portal de Administrador
        if ($rol == $usuari) {
            return $this->redirectToRoute('indexUser', ['id'=>$id]);
        }
        else if (strcmp ($rol , $admin ) == 0) {
            return $this->redirectToRoute('mostrarOfertasSinValidar');
        }

       // return $this->redirectToRoute('indexUser', ['id'=>$id]);

    }
}