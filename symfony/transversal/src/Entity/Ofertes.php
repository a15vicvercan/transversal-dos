<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfertesRepository")
 */
class Ofertes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $titol;

    /**
     * @ORM\Column(type="string", length=1000)
     */
    private $descripcio;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $data_publicacio;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ubicacio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria", inversedBy="ofertes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nombre_empresa;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $correo;

    /**
     * @ORM\Column(type="boolean")
     */
    private $validada;

    /**
     * @ORM\Column(type="integer", options={"default":0})
     */
    private $inscrits;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidatOferta", mappedBy="id_oferta")
     */
    private $candidatOfertas;

    public function __construct()
    {
        $this->candidatOfertas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitol(): ?string
    {
        return $this->titol;
    }

    public function setTitol(string $titol): self
    {
        $this->titol = $titol;

        return $this;
    }

    public function getDescripcio(): ?string
    {
        return $this->descripcio;
    }

    public function setDescripcio(string $descripcio): self
    {
        $this->descripcio = $descripcio;

        return $this;
    }

    public function getDataPublicacio(): ?\DateTimeInterface
    {
        return $this->data_publicacio;
    }

    public function setDataPublicacio(?\DateTimeInterface $data_publicacio): self
    {
        $this->data_publicacio = $data_publicacio;

        return $this;
    }

    public function getUbicacio(): ?string
    {
        return $this->ubicacio;
    }

    public function setUbicacio(string $ubicacio): self
    {
        $this->ubicacio = $ubicacio;

        return $this;
    }

    public function getCategoria(): ?Categoria
    {
        return $this->categoria;
    }

    public function setCategoria(?Categoria $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getNombreEmpresa(): ?string
    {
        return $this->nombre_empresa;
    }

    public function setNombreEmpresa(string $nombre_empresa): self
    {
        $this->nombre_empresa = $nombre_empresa;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getValidada(): ?bool
    {
        return $this->validada;
    }

    public function setValidada(bool $validada): self
    {
        $this->validada = $validada;

        return $this;
    }

    public function getInscrits(): ?int
    {
        return $this->inscrits;
    }

    public function setInscrits(int $inscrits): self
    {
        $this->inscrits = $inscrits;

        return $this;
    }

    /**
     * @return Collection|CandidatOferta[]
     */
    public function getCandidatOfertas(): Collection
    {
        return $this->candidatOfertas;
    }

    public function addCandidatOferta(CandidatOferta $candidatOferta): self
    {
        if (!$this->candidatOfertas->contains($candidatOferta)) {
            $this->candidatOfertas[] = $candidatOferta;
            $candidatOferta->setIdOferta($this);
        }

        return $this;
    }

    public function removeCandidatOferta(CandidatOferta $candidatOferta): self
    {
        if ($this->candidatOfertas->contains($candidatOferta)) {
            $this->candidatOfertas->removeElement($candidatOferta);
            // set the owning side to null (unless already changed)
            if ($candidatOferta->getIdOferta() === $this) {
                $candidatOferta->setIdOferta(null);
            }
        }

        return $this;
    }

}
