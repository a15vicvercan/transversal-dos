<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ofertes", mappedBy="categoria")
     */
    private $ofertes;

    public function __construct()
    {
        $this->ofertes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }


    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|Ofertes[]
     */
    public function getOfertes(): Collection
    {
        return $this->ofertes;
    }

    public function addOferte(Ofertes $oferte): self
    {
        if (!$this->ofertes->contains($oferte)) {
            $this->ofertes[] = $oferte;
            $oferte->setCategoria($this);
        }

        return $this;
    }

    public function removeOferte(Ofertes $oferte): self
    {
        if ($this->ofertes->contains($oferte)) {
            $this->ofertes->removeElement($oferte);
            // set the owning side to null (unless already changed)
            if ($oferte->getCategoria() === $this) {
                $oferte->setCategoria(null);
            }
        }

        return $this;
    }
}
