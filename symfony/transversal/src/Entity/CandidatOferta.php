<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatOfertaRepository")
 */
class CandidatOferta
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\Column(type="datetime")
     */
    private $data_inscripcio;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $carta_presentacio;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ofertes", inversedBy="candidatOfertas")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $id_oferta;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Candidat", inversedBy="candidatOfertas")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ip;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDataInscripcio(): ?\DateTimeInterface
    {
        return $this->data_inscripcio;
    }

    public function setDataInscripcio(\DateTimeInterface $data_inscripcio): self
    {
        $this->data_inscripcio = $data_inscripcio;

        return $this;
    }

    public function getCartaPresentacio(): ?string
    {
        return $this->carta_presentacio;
    }

    public function setCartaPresentacio(?string $carta_presentacio): self
    {
        $this->carta_presentacio = $carta_presentacio;

        return $this;
    }

    public function getIdOferta(): ?Ofertes
    {
        return $this->id_oferta;
    }

    public function setIdOferta(?Ofertes $id_oferta): self
    {
        $this->id_oferta = $id_oferta;

        return $this;
    }

    public function getUsername(): ?Candidat
    {
        return $this->username;
    }

    public function setUsername(?Candidat $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }
}
