<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CandidatRepository")
 */
class Candidat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $username;

    /**
     * @ORM\Column(type="string")
     */
    private $numeroId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message="Por favor inserta tu currículum")
     * @Assert\File(mimeTypes={"aplication/pdf"})
     */
    private $link_cv;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $correu;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CandidatOferta", mappedBy="username")
     */
    private $candidatOfertas;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $rol;

    public function __construct()
    {
        $this->candidatOfertas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getNumeroId(): ?string
    {
        return $this->numeroId;
    }

    public function setNumeroId(string $numeroId): self
    {
        $this->numeroId = $numeroId;

        return $this;
    }

    public function getLinkCv(): ?string
    {
        return $this->link_cv;
    }

    public function setLinkCv(?string $link_cv): self
    {
        $this->link_cv = $link_cv;

        return $this;
    }

    public function getCorreu(): ?string
    {
        return $this->correu;
    }

    public function setCorreu(?string $correu): self
    {
        $this->correu = $correu;

        return $this;
    }

    /**
     * @return Collection|CandidatOferta[]
     */
    public function getCandidatOfertas(): Collection
    {
        return $this->candidatOfertas;
    }

    public function addCandidatOferta(CandidatOferta $candidatOferta): self
    {
        if (!$this->candidatOfertas->contains($candidatOferta)) {
            $this->candidatOfertas[] = $candidatOferta;
            $candidatOferta->setUsername($this);
        }

        return $this;
    }

    public function removeCandidatOferta(CandidatOferta $candidatOferta): self
    {
        if ($this->candidatOfertas->contains($candidatOferta)) {
            $this->candidatOfertas->removeElement($candidatOferta);
            // set the owning side to null (unless already changed)
            if ($candidatOferta->getUsername() === $this) {
                $candidatOferta->setUsername(null);
            }
        }

        return $this;
    }

    public function getRol(): ?string
    {
        return $this->rol;
    }

    public function setRol(string $rol): self
    {
        $this->rol = $rol;

        return $this;
    }
}
