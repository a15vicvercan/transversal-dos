/*Create*/
CREATE TABLE categoria 
  ( 
     id     INT auto_increment NOT NULL, 
     nombre VARCHAR(6) NOT NULL, 
     PRIMARY KEY(id) 
  ) 
DEFAULT CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci 
engine = innodb; 

ALTER TABLE ofertes 
  ADD CONSTRAINT fk_2ca5e0f73397707a FOREIGN KEY (categoria_id) REFERENCES 
  categoria (id); 

/*Insert */

INSERT INTO `categoria`  (nombre) VALUES("DAW"),("DAM"),("ASIX"), ("SMX");
