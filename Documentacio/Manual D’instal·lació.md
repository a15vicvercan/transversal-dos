# Manual d'ús

Només obrir la nostra aplicació, a l'índex hi trobem:
    - Un gràfic mostrant el percentatge d'ofertes que hi ha de cada categoria
    - Un comptador amb el numero d'ofertes per categoria
    - Tres botons al header (Ets empresa? , Sign in i Sign out).

Si una empresa vol crear una oferta, només ha de fer click al link "Ets empresa?" que es troba en el header del índex. Això li portarà a una altra pàgina on apareixerà un formulari que ha d'emplenar amb el títol i descripció de l'oferta, ubicació, categoria i correu de l'empresa. Per a confirmar, només ha de fer click al botó "Crear oferta".

Tant per a accedir al panell d'administrador com al panell d'usuari, s'ha de fer click en el link "Sign In".

Per a accedir al panell d'administrador, s'ha d'accedir amb un correu que es trobi en la bbdd i que tingui en l'atribut rol el valor "admin". Quan ja ha fet login, el primer que pot veure són les ofertes que estan pendents de validar, aquestes es poden validar o descartar.

També pot veure les ofertes que estan validades fent click en el link del header "Ofertes validades". Aquí poden veure totes les ofertes validades, editar-les, descartar-les i veure les persones que s'han inscrit a una oferta.

Si fas login amb un usuari que no existeix, el crea en la bbdd i per defecte li posa en l'atribut rol del valor "user", perquè quan faci login en comptes d'accedir al panell d'administrador accedeixi al panell d'usuari.

Si l'usuari ja existeix i el seu rol és "user", li mostrés el panell d'usuari.

En el panell d'usuari, es mostren les ofertes que estan validades i la seva antiguitat és inferior a 3 mesos. Les ofertes amb menys de 15 d'antiguitat es mostren de color taronja, i les que té més de 15 dies de color marró.

També pot filtrar per categoria.

L'usuari pot pujar el seu CV perquè quan s'inscrigui a una oferta també se li enviï a l'empresa el seu CV. Per a fer-ho ha de fer click en el botó "Penja el CV", llavors apareixerà un modal on podrà pujar el CV i guardar-lo.

Si l'usuari no aquesta inscrit en una oferta i fa click en la barra on està el títol de l'oferta, s'obre un desplegable amb la informació de l'oferta, un input per a introduir la carta de presentació, una casella de selecció advertint de la RGPD i el botó d'inscriure's.

Quan s'inscriu, a l'empresa que ha creat aquesta oferta se li envia en email amb la informació de l'usuari, la carta de presentació, el CV i el titulo de l'oferta a la qual s'ha inscrit.

Si l'usuari està inscrit en alguna oferta, es mostrarà un punt blau en el títol de l'oferta, podrà veure la informació de l'oferta però no li apareixerà ni l'input per a introduir la carta de presentació ni el botó d'inscriure's.


# Manual D'instal·lació

Per poder desplegar la nostra aplicació en apache, primer hem d'agafar la URL del nostre repositori per poder clonar-lo al directori /var/www/html.

A continuació, accedim al fitxer .env posem les variables APP_ENV=*prod i APP_DEBUG=0, i executem la següent comanda: "composer install --no-dev --optimize-autoloader".

També hem d'instal·lar un component de symfony per a apache, que crea un .htaccess ja configurat per al site amb la següent comanda: "composer require symfony/apache-pack". Mentre s'instal·la, preguntarà si volem que creï el htaccess, hem de seleccionar l'opció si.

Després hem de netejar la cache amb la comanda: "APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear"

Per a configurar la bbdd, primer hem de crear la bbdd amb la comanda: "php bin/console doctrine:database:create".

A continuació s'ha d'executar la següent comanda perquè creï un fitxer "migrations" que conté el SQL per a crear les taules: "php bin/console doctrine:migrations:diff". I seguidament per a executar aquest sql s'ha d'usar aquesta comanda: "php bin/console doctrine:migrations:migrate".

Per finalitzar, perquè des del navegador es pugui visualitzar y que les funcions de penjar el CV i enviar el mail funcionin correctament, se li ha de donar permisos al directori, per fer-ho fem: chmod -R 777 transversal-dos.