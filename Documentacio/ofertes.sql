/*Create*/
CREATE TABLE ofertes 
  ( 
     id              INT auto_increment NOT NULL, 
     categoria_id    INT NOT NULL, 
     titol           VARCHAR(50) NOT NULL, 
     descripcio      VARCHAR(1000) NOT NULL, 
     data_publicacio DATETIME DEFAULT NULL, 
     ubicacio        VARCHAR(50) NOT NULL, 
     nombre_empresa  VARCHAR(50) NOT NULL, 
     correo          VARCHAR(50) NOT NULL, 
     INDEX idx_2ca5e0f73397707a (categoria_id), 
     PRIMARY KEY(id) 
  ) 
DEFAULT CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci 
engine = innodb

/*Insert */
